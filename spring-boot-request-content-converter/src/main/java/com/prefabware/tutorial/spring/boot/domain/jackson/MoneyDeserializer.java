package com.prefabware.tutorial.spring.boot.domain.jackson;

import java.io.IOException;
import java.math.BigDecimal;
import javax.money.Monetary;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.javamoney.moneta.Money;

/**
 * to deserialize an Instant from a long with milliseconds since epoch
 */
public class MoneyDeserializer extends JsonDeserializer<Money> {

	@Override
	public Money deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		JsonNode node = jp.getCodec().readTree(jp);
		String value = (node.get("value").textValue());
		String currency = (node.get("currency").textValue());
		return Money.of(new BigDecimal(value),Monetary.getCurrency(currency));
	}

	@Override
	public Class<?> handledType() {
		return Money.class;
	}
}
