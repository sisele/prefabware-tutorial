package com.prefabware.tutorial.spring.boot.repository;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by stefan on 27.12.17.
 */
@Configuration
public class PaymentRepositoryConfig {
	@Bean
	PaymentRepository paymentRepository(){return new PaymentRepository();}
}
