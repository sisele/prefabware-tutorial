package com.prefabware.tutorial.spring.boot.service;

import java.time.Instant;

import com.prefabware.tutorial.spring.boot.domain.Payment;
import com.prefabware.tutorial.spring.boot.domain.PaymentRequest;
import com.prefabware.tutorial.spring.boot.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by stefan on 27.12.17.
 */
public class PaymentService {
	@Autowired
	PaymentRepository repository;

	public Payment pay(PaymentRequest paymentRequest){
		// communication with payment provider not implemented ...
		return repository.save(new Payment(paymentRequest, Instant.now()));
	}
}
