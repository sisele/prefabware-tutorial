package com.prefabware.tutorial.spring.boot.domain;

import java.time.Instant;
import java.util.Objects;

import static org.springframework.util.Assert.notNull;

/**
 * Created by stefan on 27.12.17.
 */
public class Payment {
	public final PaymentId id;
	public final PaymentRequest request;
	public final Instant paidAt;

	public Payment(PaymentRequest request, Instant paidAt) {
		// chain constructors to avoid code duplication
		this(null,request,paidAt);
	}

	public Payment(PaymentId id, PaymentRequest request, Instant paidAt) {
		notNull(request, "request must not be null");
		notNull(paidAt, "paidAt must not be null");
		this.id=id;
		this.request = request;
		this.paidAt = paidAt;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Payment payment = (Payment) o;
		return Objects.equals(request, payment.request) &&
				Objects.equals(paidAt, payment.paidAt);
	}

	@Override
	public int hashCode() {
		return Objects.hash(request, paidAt);
	}

	public PaymentId getId() {
		return id;
	}

	public Instant getPaidAt() {
		return paidAt;
	}

	public PaymentRequest getRequest() {
		return request;
	}
}
