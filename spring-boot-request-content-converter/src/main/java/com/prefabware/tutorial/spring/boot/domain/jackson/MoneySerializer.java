package com.prefabware.tutorial.spring.boot.domain.jackson;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.javamoney.moneta.Money;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.*;

public class MoneySerializer extends JsonSerializer<Money> {
	@JsonAutoDetect(fieldVisibility = ANY,getterVisibility = NONE,setterVisibility = NONE)
	static class MoneyJson {
		@JsonCreator
		MoneyJson(@JsonProperty("currency") String currency,@JsonProperty("value") String value) {
			this.currency = currency;
			this.value = value;
		}

		/**
		 * to prevent rounding errors and loss of precision, amount of money
		 * shouldbe serialized as string,see also
		 * https://github.com/zalando/jackson-datatype-money/blob/master/MONEY.md
		 */
		final String value;
		final String currency;

		public MoneyJson(Money money) {
			this(money.getCurrency().getCurrencyCode(),money.getNumber().toString());
		}
	}

	@Override
	public void serialize(Money money, JsonGenerator jsonGenerator,
						  SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
		MoneyJson json=new MoneyJson(money);
		jsonGenerator.writeObject(json);
	}

	/**
	 * the supermethod returns null,you need to override this, if you want to register
	 * using org.springframework.http.converter.json.Jackson2ObjectMapperBuilder#serializers
	 * @return
	 */
	@Override
	public Class<Money> handledType() {
		return Money.class;
	}
}