package com.prefabware.tutorial.spring.boot.domain;


import java.util.regex.Pattern;

import com.prefabware.tec.commons.spring.string.CharSequenceBaseJson;
import org.springframework.util.Assert;

/**
 * the id ofan payment must be exactly 10 digits long
 * only numbers are allowed.
 * To avoid spreading this logic all around the codebase, use
 * always this class
 */
public class PaymentId extends CharSequenceBaseJson {
	public static final String NOT_VALID = "Not a valid PaymentId: ";
	static Pattern pattern=Pattern.compile("\\d{10}");

	public PaymentId(CharSequence value) {
		super(value);
		// validate the required format
		Assert.isTrue(pattern.matcher(value).matches(), NOT_VALID +value);
	}

	/**
	 * only for spring to convert controller method parms automatically
	 * @param value
	 */
	public PaymentId(String value) {
		this((CharSequence)value);
	}

	public static PaymentId of(CharSequence id) {
		return new PaymentId(id);
	}
}
