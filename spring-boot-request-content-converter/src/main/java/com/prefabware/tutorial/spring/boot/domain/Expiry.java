package com.prefabware.tutorial.spring.boot.domain;

import java.time.Month;
import java.time.Year;
import java.time.YearMonth;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.prefabware.tec.commons.spring.string.CharSequenceBaseJson;
import org.springframework.util.Assert;

/**
 * Created by stefan on 27.12.17.
 */
public class Expiry extends CharSequenceBaseJson {
	static final Pattern pattern = Pattern.compile("(?<MM>0[1-9]|1[012])/(?<yy>[0-9][0-9])");
	public static final String NOT_VALID = "Not a valid creditcard expiry";

	public final YearMonth yearMonth;

	public static Expiry of(String value) {
		return new Expiry(value);
	}

	public Expiry(CharSequence value) {
		super(value);
		Matcher matcher = pattern.matcher(value);
		Assert.isTrue(matcher.matches(), NOT_VALID +" "+ value);

		matcher.reset();
		matcher.find();

		String yy = matcher.group("yy");
		Year year = Year.of(2000 + Integer.parseInt(yy));

		String MM = matcher.group("MM");
		Month month = Month.of(Integer.parseInt(MM));

		yearMonth = YearMonth.of(year.getValue(), month);
	}

	public static Expiry of(YearMonth yearMonth) {
		Assert.isTrue(yearMonth.getYear()>=2000,"year must be greater or equal 2000 bit is "+ yearMonth.getYear());
		int yy = (yearMonth.getYear() - 2000);
		int MM = yearMonth.getMonthValue();
		return new Expiry(String.format("%02d/%02d", MM, yy));
	}
}
