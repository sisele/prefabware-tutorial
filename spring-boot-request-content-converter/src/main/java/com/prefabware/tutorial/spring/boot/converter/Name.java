package com.prefabware.tutorial.spring.boot.converter;

/**
 * Created by stefan on 22.10.17.
 */
public class Name {
	public Name(CharSequence chars) {
		this.chars = chars;
	}

	final CharSequence chars;

	@Override
	public String toString() {
		return "Name{" +
				"chars=" + chars +
				'}';
	}
}
