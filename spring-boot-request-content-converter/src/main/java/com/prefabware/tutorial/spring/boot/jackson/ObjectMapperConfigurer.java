package com.prefabware.tutorial.spring.boot.jackson;

import java.time.Instant;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.prefabware.tutorial.spring.boot.domain.jackson.InstantDeserializer;
import com.prefabware.tutorial.spring.boot.domain.jackson.InstantSerializer;
import com.prefabware.tutorial.spring.boot.domain.jackson.MoneyDeserializer;
import com.prefabware.tutorial.spring.boot.domain.jackson.MoneySerializer;
import org.javamoney.moneta.Money;

/**
 * Created by stefan on 28.12.17.
 */
public class ObjectMapperConfigurer {
	public static void configure(ObjectMapper mapper){
		SimpleModule module = new SimpleModule();
		module.addSerializer(Money.class, new MoneySerializer());
		module.addDeserializer(Money.class, new MoneyDeserializer());
		module.addSerializer(Instant.class, new InstantSerializer());
		module.addDeserializer(Instant.class, new InstantDeserializer());
		mapper.registerModule(module);
	}
}
