package com.prefabware.tutorial.spring.boot.api;

import java.net.URI;

import com.prefabware.tutorial.spring.boot.domain.Payment;
import com.prefabware.tutorial.spring.boot.domain.PaymentId;
import com.prefabware.tutorial.spring.boot.domain.PaymentRequest;
import com.prefabware.tutorial.spring.boot.repository.PaymentRepository;
import com.prefabware.tutorial.spring.boot.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/payment")
public class PaymentController {

	@Autowired
	PaymentService service;

	@Autowired
	PaymentRepository repository;

	@Autowired
	PaymentResourceAssembler assembler;

	@RequestMapping(method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
	// @RequestBody is mandatory !!! to map the body !!
	public ResponseEntity<?> post(@RequestBody PaymentRequest paymentRequest) {
		Payment payment = service.pay(paymentRequest);
		PaymentResource resource = assembler.toResource(payment);
		URI location = URI.create(resource.getLink(Link.REL_SELF)
				.getHref());
		return ResponseEntity.created(location)
				.build();
	}

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<PaymentResource> get(@PathVariable("id") PaymentId id) {
		Payment payment = repository.find(id);
		if(payment==null){return ResponseEntity.notFound().build();}
		PaymentResource resource = assembler.toResource(payment);
		return ResponseEntity.ok(resource);
	}

	/**
	 *
	 * @param id
	 * @return the deleted entity,as recomended by https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.7/
	 */
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<PaymentResource> delete(@PathVariable("id") PaymentId id) {
		Payment payment=repository.delete(id);
		PaymentResource resource = assembler.toResource(payment);
		return ResponseEntity.ok(resource);
	}
}