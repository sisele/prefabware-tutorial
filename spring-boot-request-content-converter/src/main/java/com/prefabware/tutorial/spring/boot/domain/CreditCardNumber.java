package com.prefabware.tutorial.spring.boot.domain;

import java.util.regex.Pattern;

import com.prefabware.tec.commons.string.CharSequenceBase;
import org.springframework.util.Assert;

/**
 * Created by stefan on 27.12.17.
 */
public class CreditCardNumber extends CharSequenceBase {

	static final String regex = "^(?:(?<visa>4[0-9]{12}(?:[0-9]{3})?)|" +
			"(?<mastercard>5[1-5][0-9]{14})|" +
			"(?<discover>6(?:011|5[0-9]{2})[0-9]{12})|" +
			"(?<amex>3[47][0-9]{13})|" +
			"(?<diners>3(?:0[0-5]|[68][0-9])?[0-9]{11})|" +
			"(?<jcb>(?:2131|1800|35[0-9]{3})[0-9]{11}))$";

	static final Pattern pattern = Pattern.compile(regex);
	public static final String NOT_A_VALID_CREDIT_CARD_NUMBER = "not a valid credit card number ";

	static boolean matches(CharSequence value) {
		if(value==null){return false;}
		//Strip all hyphens
		String stripped = value.toString().replaceAll("-", "");
		return pattern.matcher(stripped)
				.matches();
	}

	public static CreditCardNumber of(String value) {
		return new CreditCardNumber(value);
	}

	public CreditCardNumber(CharSequence value) {
		super(value);
		Assert.isTrue(matches(value), NOT_A_VALID_CREDIT_CARD_NUMBER +value);
	}
}
