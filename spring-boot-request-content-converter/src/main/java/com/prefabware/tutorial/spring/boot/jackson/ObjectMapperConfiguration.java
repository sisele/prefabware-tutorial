package com.prefabware.tutorial.spring.boot.jackson;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * Created by stefan on 02.01.18.
 */
@Configuration
class ObjectMapperConfiguration {
	@Autowired
	ObjectMapper objectMapper;
	@PostConstruct
	void postConstruct(){
		ObjectMapperConfigurer.configure(objectMapper);
	}

}
