package com.prefabware.tutorial.spring.boot.api;

import java.time.Instant;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.prefabware.tutorial.spring.boot.domain.PaymentRequest;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.util.Assert.notNull;

/**
 * Created by stefan on 27.12.17.
 */
public class PaymentResource extends ResourceSupport {
	public final PaymentRequest request;
	public final Instant paidAt;

	@JsonCreator
	public PaymentResource(@JsonProperty("request") PaymentRequest request,@JsonProperty("paidAt") Instant paidAt) {
		notNull(request, "request must not be null");
		notNull(paidAt, "paidAt must not be null");
		this.request = request;
		this.paidAt = paidAt;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		PaymentResource that = (PaymentResource) o;
		return Objects.equals(request, that.request) &&
				Objects.equals(paidAt, that.paidAt);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), request, paidAt);
	}
}
