package com.prefabware.tutorial.spring.boot.api;

import java.time.Instant;
import javax.money.CurrencyUnit;
import javax.money.Monetary;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prefabware.tutorial.spring.boot.domain.*;
import com.prefabware.tutorial.spring.boot.repository.PaymentRepository;
import com.prefabware.tutorial.spring.boot.service.PaymentService;
import org.javamoney.moneta.Money;
import org.junit.*;
import org.junit.runner.*;
import org.mockito.Mockito;
import org.mockito.configuration.MockitoConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.Assert.*;
import static org.mockito.Matchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Created by stefan on 27.12.17.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(PaymentController.class)
@Import({PaymentControllerTest.Config.class})
public class PaymentControllerTest {
	@Configuration
	@Import({PaymentApiConfig.class})
	static class Config {
		@MockBean
		PaymentService paymentService;

		@MockBean
		PaymentRepository repository;
	}

	@Autowired
	MockMvc mvc;

	@Autowired
	PaymentRepository repository;
	@Autowired
	PaymentService paymentService;
	@Autowired
	ObjectMapper objectMapper;

	CurrencyUnit usd;
	Money twelveUsd;
	CreditCardNumber creditCardNumber;
	Expiry expiry;
	Instant date;
	PaymentRequest paymentRequest;
	Payment payment;

	@Before
	public void setUp() throws Exception {
		MockitoConfiguration.stricktMocking = true;

		CurrencyUnit usd = Monetary.getCurrency("USD");
		twelveUsd = Money.of(12, usd);
		creditCardNumber = CreditCardNumber.of("3782-8224-6310-005");
		expiry = Expiry.of("08/17");
		date = Instant.parse("2016-12-31T10:00:00Z");
		paymentRequest = new PaymentRequest(creditCardNumber, expiry, twelveUsd, date);
		payment = new Payment(PaymentId.of("0000000001"), paymentRequest, Instant.now());
	}

	@Test
	public void testGet() throws Exception {
		Mockito.doReturn(payment)
				.when(repository)
				.find(PaymentId.of("0000000001"));

		MvcResult result = mvc.perform(get("/payment/0000000001")
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		String content = result.getResponse()
				.getContentAsString();
		PaymentResource resource = objectMapper.readValue(content, PaymentResource.class);
		assertEquals(paymentRequest, resource.request);
	}

	@Test
	public void testPost() throws Exception {
		Mockito.doReturn(payment)
				.when(paymentService)
				.pay(eq(paymentRequest));


		String content = objectMapper.writeValueAsString(paymentRequest);
		MvcResult result = mvc.perform(post("/payment")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(content))
				.andDo(print())
				.andExpect(status().isCreated())
				.andExpect(header().string("Location", "http://localhost/payment/0000000001"))
				.andReturn();
	}
}