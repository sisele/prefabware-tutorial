package com.prefabware.tutorial.spring.boot.api.config;

import java.util.List;

import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.zalando.jackson.datatype.money.MoneyModule;

/**
 * Created by stefan on 27.03.18.
 */
@Component
public class MoneyModuleWebMvcConfigurer extends WebMvcConfigurerAdapter {
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		Jackson2ObjectMapperBuilder builder=new Jackson2ObjectMapperBuilder();
		builder.modules(new MoneyModule());
		super.configureMessageConverters(converters);
	}
}
