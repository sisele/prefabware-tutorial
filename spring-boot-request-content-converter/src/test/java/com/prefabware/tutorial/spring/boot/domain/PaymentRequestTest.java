package com.prefabware.tutorial.spring.boot.domain;

import java.time.Instant;
import javax.money.CurrencyUnit;
import javax.money.Monetary;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prefabware.tutorial.spring.boot.jackson.ObjectMapperConfigurer;
import org.javamoney.moneta.Money;
import org.junit.*;

import static org.junit.Assert.*;

/**
 * Created by stefan on 27.12.17.
 */
public class PaymentRequestTest {
	public static final String JSON = "{'creditCardNumber':'3782-8224-6310-005','expiry':'08/17','amount':{'currency':'USD','value':'12'},'date':'2016-12-31T10:00:00Z'}".replace(
			"'", "\"");
	CurrencyUnit usd;
	Money twelveUsd;
	CreditCardNumber creditCardNumber;
	Expiry expiry;
	Instant date;
	ObjectMapper mapper;

	@Before
	public void setUp() throws Exception {
		CurrencyUnit usd = Monetary.getCurrency("USD");
		twelveUsd = Money.of(12, usd);
		creditCardNumber = CreditCardNumber.of("3782-8224-6310-005");
		expiry = Expiry.of("08/17");
		date = Instant.parse("2016-12-31T10:00:00Z");

		mapper = new ObjectMapper();
		ObjectMapperConfigurer.configure(mapper);
	}

	@Test
	public void testOk() throws Exception {
		PaymentRequest p = new PaymentRequest(creditCardNumber, expiry, twelveUsd, date);
	}

	@Test
	public void testSerialize() throws Exception {
		PaymentRequest p = new PaymentRequest(creditCardNumber, expiry, twelveUsd, date);
		String json = mapper.writeValueAsString(p);
		assertEquals(JSON, json);
	}

	@Test
	public void testDeserialize() throws Exception {
		PaymentRequest cc = mapper.readValue(JSON, PaymentRequest.class);
		PaymentRequest expected = new PaymentRequest(creditCardNumber, expiry, twelveUsd, date);
		assertEquals(expected, cc);
	}
}