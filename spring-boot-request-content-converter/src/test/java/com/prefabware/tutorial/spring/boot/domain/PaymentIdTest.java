package com.prefabware.tutorial.spring.boot.domain;

import java.util.function.Predicate;

import org.junit.*;

import static org.junit.Assert.*;

/**
 * Created by stefan on 02.01.18.
 */
public class PaymentIdTest {

	@Test
	public void ofOk() throws Exception {
		assertEquals("1234567890",PaymentId.of("1234567890").toString());
	}
	@Test
	public void ofFails() throws Exception {
		Runnable runnable = ()->PaymentId.of(null);
		assertFails(runnable,(e)->e.getClass()==IllegalArgumentException.class && e.getMessage().contains("the value must not be null"));
		assertFails(()->PaymentId.of(""));
		assertFails(()->PaymentId.of(" "));
		assertFails(()->PaymentId.of("1 34567890"));
		assertFails(()->PaymentId.of("11234567890"));
		assertFails(()->PaymentId.of("12a4567890"));
	}

	void assertFails(Runnable runnable){
		assertFails(runnable,(e)->e.getClass()==IllegalArgumentException.class && e.getMessage().contains(PaymentId.NOT_VALID));
	}

	void assertFails(Runnable runnable,Predicate<Throwable> predicate){
		try {
			runnable.run();
			fail("Exception expected");
		}catch (Throwable t){
			assertTrue("predicate returned false for "+t,predicate.test(t));
		}
	}
}