package com.prefabware.tutorial.spring.boot.api;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.Instant;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.jayway.jsonpath.JsonPath;
import com.prefabware.tutorial.spring.boot.domain.jackson.InstantDeserializer;
import com.prefabware.tutorial.spring.boot.domain.jackson.InstantSerializer;
import com.prefabware.tutorial.spring.boot.domain.jackson.MoneyDeserializer;
import com.prefabware.tutorial.spring.boot.domain.jackson.MoneySerializer;
import com.prefabware.tutorial.spring.boot.test.PaymentTestFactory;
import org.javamoney.moneta.Money;
import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StreamUtils;

import static org.junit.Assert.*;

/**
 * Created by stefan on 28.12.17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {PaymentResourceTest.Config.class})
public class PaymentResourceTest {
	@Configuration
	@Import({})
	static class Config {

		@Bean
		ObjectMapper objectMapper() {
			ObjectMapper mapper = new ObjectMapper();
			SimpleModule module = new SimpleModule();
			module.addSerializer(Money.class, new MoneySerializer());
			module.addDeserializer(Money.class, new MoneyDeserializer());
			module.addSerializer(Instant.class, new InstantSerializer());
			module.addDeserializer(Instant.class, new InstantDeserializer());
			mapper.registerModule(module);
			//mapper.findAndRegisterModules();
			return mapper;
		}
	}
	PaymentTestFactory factory = new PaymentTestFactory();

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	ResourceLoader loader;

	Resource resource;

	String json;

	@Before
	public void setUp() throws Exception {
		resource = loader.getResource(
				"classpath:com/prefabware/tutorial/spring/boot/test/payment-factory.json");
			InputStream stream = resource.getInputStream();
			json = StreamUtils.copyToString(stream, StandardCharsets.UTF_8);
	}

	@Test
	public void deserialize() throws Exception {
		PaymentResource deserialized = objectMapper.readValue(json, PaymentResource.class);
		assertEquals(factory.payment.paidAt,deserialized.paidAt);
		assertEquals(factory.paymentRequest,deserialized.request);
	}

	@Test
	public void serialize() throws Exception {
		PaymentResource resource = new PaymentResource(factory.paymentRequest,factory.paidAt);
		String json = objectMapper.writeValueAsString(resource);
		assertEquals("3782-8224-6310-005", JsonPath.parse(json)
				.read("$.request.creditCardNumber"));
		assertEquals("3782-8224-6310-005", JsonPath.parse(json)
				.read("$.request.creditCardNumber"));
		assertEquals("08/17", JsonPath.parse(json)
				.read("$.request.expiry"));
		assertEquals("12", JsonPath.parse(json)
				.read("$.request.amount.value"));
		assertEquals("USD", JsonPath.parse(json)
				.read("$.request.amount.currency"));
		assertEquals("2016-12-28T10:00:00Z", JsonPath.parse(json)
				.read("$.request.date"));
		String iso = JsonPath.parse(json)
				.read("$.paidAt");
		assertEquals("2016-12-31T10:00:00Z", iso);

		//to test if the string has the right format to create an Instant
		Instant parsed = Instant.parse(iso);
	}
}