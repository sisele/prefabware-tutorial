package com.prefabware.tutorial.spring.boot.api.standard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * to be used in junit tests
 * a SpringBootApplication class must be used in the configuration or property file  handling doesnt work properly
 * @author stefan
 * 
 */
@Configuration
@EnableAutoConfiguration
public class SpringBootTestApplication {

	/**
     * spring accidentally creates two different beans of this type which leads to an exception
     * see https://github.com/spring-projects/spring-boot/issues/1539
     * this primary fixes it
     * @return
     */
    @Bean
    @Primary
    public Validator localValidatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringBootTestApplication.class);
    }

}
