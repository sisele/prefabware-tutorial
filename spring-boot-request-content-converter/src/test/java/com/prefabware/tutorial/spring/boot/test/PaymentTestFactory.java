package com.prefabware.tutorial.spring.boot.test;

import java.time.Instant;
import javax.money.CurrencyUnit;
import javax.money.Monetary;

import com.prefabware.tutorial.spring.boot.domain.*;
import org.javamoney.moneta.Money;

/**
 * Created by stefan on 28.12.17.
 */
public class PaymentTestFactory {
	public final CurrencyUnit usd;
	public final Money twelveUsd;
	public final CreditCardNumber creditCardNumber;
	public final Expiry expiry;
	public final Instant date;
	public final PaymentRequest paymentRequest;
	public final Payment payment;
	public final Instant paidAt;

	public PaymentTestFactory() {
		usd = Monetary.getCurrency("USD");
		twelveUsd = Money.of(12, usd);
		creditCardNumber = CreditCardNumber.of("3782-8224-6310-005");
		expiry = Expiry.of("08/17");
		date = Instant.parse("2016-12-28T10:00:00Z");
		paymentRequest = new PaymentRequest(creditCardNumber, expiry, twelveUsd, date);
		paidAt = Instant.parse("2016-12-31T10:00:00Z");
		PaymentId id = PaymentId.of("0000000001");
		payment = payment(id);
	}

	public Payment payment(PaymentId id) {
		return new Payment(id, paymentRequest, paidAt);
	}
}
