package com.qvc.finance.payment.common.domain.base;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.util.Assert;

/**
 * base class for character codes
 */
public abstract class CharacterCode {
    public static final String VALUE_MUST_NOT_BE_NULL = "value must not be null";

    /**
     * the value this code is based on
     */
    CharSequence value;

    /**
     *
     * @param value, the value this code is based on, must not be null
     */
    @JsonCreator
    public CharacterCode(CharSequence value) {
        Assert.notNull(value, VALUE_MUST_NOT_BE_NULL);
        this.value = value;
    }

    /**
     * two codes are equal, if they are based on the same value
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CharacterCode that = (CharacterCode) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    /**
     * @return the string and JSON representation of this code.
     */
    @JsonValue
    @Override
    public String toString() {
        return value.toString();
    }
}
