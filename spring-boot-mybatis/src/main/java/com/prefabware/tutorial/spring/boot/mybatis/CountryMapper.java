package com.prefabware.tutorial.spring.boot.mybatis;

import org.apache.ibatis.annotations.*;

@Mapper
public interface CountryMapper {

	@InsertProvider(type=CountrySqlBuilder.class,method="insert")
	@Options(useGeneratedKeys = true)
	Integer insert(Country country);

	/**
	 * @ConstructorArgs see https://dzone.com/articles/ibatis-mybatis-handling
	 * @param code
	 * @return
	 */
    @Select("SELECT * FROM Country WHERE code = #{code}")
	@ConstructorArgs(value = {
			@Arg(column="id",javaType=Integer.class),
			@Arg(column="code",javaType=CountryCode.class),
			@Arg(column="name",javaType=String.class)
	})
	Country findByCode(@Param("code") CountryCode code);

}