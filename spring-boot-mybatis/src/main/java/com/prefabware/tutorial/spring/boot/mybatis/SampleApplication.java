package com.prefabware.tutorial.spring.boot.mybatis;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@SpringBootConfiguration
@EnableAutoConfiguration
public class SampleApplication implements CommandLineRunner {

    private final CountryMapper countryMapper;

    public SampleApplication(CountryMapper countryMapper) {
        this.countryMapper = countryMapper;
    }

    public static void main(String[] args) {
        SpringApplication.run(SampleApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

    }

}