package com.prefabware.tutorial.spring.boot.mybatis;

import java.util.List;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.TypeHandler;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by stefan on 15.03.18.
 */
@Configuration
//@MapperScan(basePackageClasses = CountryMapper.class)
public class CountryConfig {


	@Bean
	CountryDao countryDao(SqlSession session) {
		return new CountryDao(session);
	}

	@Bean
	CountryCodeTypeHandler countryTypeHandler(){return new CountryCodeTypeHandler();} 
	
	@Bean
	public SqlSessionFactory sqlSessionFactory(DataSource dataSource,List<TypeHandler> typeHandlers) throws Exception {
		SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
		sqlSessionFactory.setDataSource(dataSource);
		sqlSessionFactory.setTypeHandlers(typeHandlers.toArray(new TypeHandler[0]));
		return (SqlSessionFactory) sqlSessionFactory.getObject();
	}
}
