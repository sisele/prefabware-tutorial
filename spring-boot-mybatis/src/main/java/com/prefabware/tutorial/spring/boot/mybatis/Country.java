package com.prefabware.tutorial.spring.boot.mybatis;

/**
 * Created by stefan on 15.03.18.
 */
public class Country {
	public static Country of(String code, String name){
		return new Country(null, CountryCode.of(code), name);
	}

	Integer id;
	CountryCode code;
	String name;
	Country(Integer id, CountryCode code, String name) {
		this.id=id;
		this.name = name;
		this.code = code;
	}
}
