package com.prefabware.tutorial.spring.boot.mybatis;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Component;

@Component
public class CountryDao {

	final SqlSession sqlSession;

	public CountryDao(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}

	public Country selectCountryById(long id) {
		return this.sqlSession.selectOne("selectCountryById", id);
	}

}