package com.prefabware.tutorial.spring.boot.mybatis;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

/**
 * http://www.mybatis.org/mybatis-3/configuration.html#typeHandlers
 * Created by stefan on 16.03.18.
 */
@MappedJdbcTypes(value=JdbcType.VARCHAR,includeNullJdbcType=true)
@MappedTypes({CountryCode.class})
public class CountryCodeTypeHandler extends CharacterCodeTypeHandler<CountryCode> {
	CountryCodeTypeHandler() {
		super(cc -> cc.toString(), s -> CountryCode.of(s));
	}
}
