package com.prefabware.tutorial.spring.boot.mybatis;
 
import java.util.regex.Pattern;

import com.qvc.finance.payment.common.domain.base.CharacterCode;
import org.springframework.util.Assert;
 
/**
 * to identify a country e.g. in a service call
 * <p>
 * ISO 3166-1 alpha-2 country code see https://en.wikipedia.org/wiki/ISO_3166-1
 * - format : must have exactly two uppercase characters
 * - possible values : are subject to changes, so must be loaded dynamically
 */
public class CountryCode extends CharacterCode {
    static final String FORMAT_NOT_VALID = "format is not valid for a ISO 3166-1 alpha-2 country code : ";
    static Pattern pattern = Pattern.compile("[A-Z][A-Z]");
 
    public static CountryCode of(CharSequence value) {
        return new CountryCode(value);
    }
 
    public CountryCode(CharSequence value) {
        super(value);
        Assert.isTrue(pattern.matcher(value).matches(), FORMAT_NOT_VALID + value);
    }
 
}