package com.prefabware.tutorial.spring.boot.mybatis;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import static org.springframework.util.Assert.notNull;

/**
 * http://www.mybatis.org/mybatis-3/configuration.html#typeHandlers
 * subclass this to handle types that have a string value
 * Created by stefan on 16.03.18.
 */
public abstract class CharacterCodeTypeHandler<T> extends BaseTypeHandler<T> {

	final Function<String, T> toType;
	final Function<T, String> toString;

	CharacterCodeTypeHandler(Function<T, String> toString, Function<String, T> toType) {
		notNull(toString, "function toString must not be null");
		notNull(toType, "function toType must not be null");
		this.toString = toString;
		this.toType = toType;
	}

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, T parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, toString.apply(parameter));
	}

	@Override
	public T getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return toType.apply(rs.getString(columnName));
	}

	@Override
	public T getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return toType.apply(rs.getString(columnIndex));
	}

	@Override
	public T getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return toType.apply(cs.getString(columnIndex));
	}
}
