package com.prefabware.tutorial.spring.boot.mybatis;

import org.apache.ibatis.jdbc.SQL;

/**
 * Created by stefan on 15.03.18.
 */
public class CountrySqlBuilder {
	static final String TABLE = "Country";

	public String insert() {
		return new SQL().
				INSERT_INTO(TABLE)
				.VALUES("code", "#{code}")
				.VALUES("name", "#{name}")
				.toString();
	}
}