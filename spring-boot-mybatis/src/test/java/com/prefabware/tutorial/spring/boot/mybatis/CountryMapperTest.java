package com.prefabware.tutorial.spring.boot.mybatis;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * http://www.mybatis.org/spring-boot-starter/mybatis-spring-boot-autoconfigure/
 * http://www.baeldung.com/mybatis
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SampleApplication.class, CountryMapperTest.Config.class})
public class CountryMapperTest {

	@Configuration
	@Import({CountryConfig.class})
	static class Config {

	}

	@Before
	public void setUp() throws Exception {
		countryMapper.insert(Country.of("DE", "Germany"));

	}

	@Autowired
	CountryMapper countryMapper;

	@Test
	public void findOk() {
		Country ca = countryMapper.findByCode(CountryCode.of("DE"));

		assertNotNull(ca);
	}
}