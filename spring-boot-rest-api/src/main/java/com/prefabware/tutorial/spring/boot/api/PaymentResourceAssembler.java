package com.prefabware.tutorial.spring.boot.api;

import com.prefabware.tutorial.spring.boot.domain.Payment;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceAssembler;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by stefan on 27.12.17.
 */
public class PaymentResourceAssembler implements ResourceAssembler<Payment,PaymentResource>{
	@Override
	public PaymentResource toResource(Payment entity) {
		PaymentResource resource = new PaymentResource(entity.request,entity.paidAt);
		// add  self link,that points to the get api
		resource.add(new Link( linkTo(methodOn(PaymentController.class).get(entity.id)).toString()).withSelfRel());
		return resource;
	}
}
