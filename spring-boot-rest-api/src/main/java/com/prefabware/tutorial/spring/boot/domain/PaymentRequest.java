package com.prefabware.tutorial.spring.boot.domain;

import java.time.Instant;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.javamoney.moneta.Money;
import org.springframework.util.Assert;

/**
 * Created by stefan on 27.12.17.
 */
@JsonAutoDetect(fieldVisibility = Visibility.PUBLIC_ONLY,getterVisibility = Visibility.NONE,setterVisibility = Visibility.NONE)
public class PaymentRequest {
	public final CreditCardNumber creditCardNumber;
	public final Expiry expiry;
	public final Money amount;
	public final Instant date;

	@JsonCreator
	public PaymentRequest(@JsonProperty("creditCardNumber") CreditCardNumber creditCardNumber,
						  @JsonProperty("expiry") Expiry expiry,
						  @JsonProperty("amount") Money amount,
						  @JsonProperty("date") Instant date) {
		Assert.notNull(creditCardNumber, " creditCardNumber must not be null");
		Assert.notNull(expiry, "expiry amount must not be null");
		Assert.notNull(amount, " amount must not be null");
		Assert.notNull(date, "date amount must not be null");

		this.amount = amount;
		this.creditCardNumber = creditCardNumber;
		this.expiry = expiry;
		this.date = date;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		PaymentRequest paymentRequest = (PaymentRequest) o;
		return Objects.equals(creditCardNumber, paymentRequest.creditCardNumber) &&
				Objects.equals(expiry, paymentRequest.expiry) &&
				Objects.equals(amount, paymentRequest.amount) &&
				Objects.equals(date, paymentRequest.date);
	}

	@Override
	public int hashCode() {
		return Objects.hash(creditCardNumber, expiry, amount, date);
	}
}
