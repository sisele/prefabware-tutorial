package com.prefabware.tutorial.spring.boot.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.prefabware.tutorial.spring.boot.domain.jackson.InstantDeserializer;
import com.prefabware.tutorial.spring.boot.domain.jackson.InstantSerializer;
import com.prefabware.tutorial.spring.boot.domain.jackson.MoneyDeserializer;
import com.prefabware.tutorial.spring.boot.domain.jackson.MoneySerializer;
import com.prefabware.tutorial.spring.boot.repository.PaymentRepository;
import com.prefabware.tutorial.spring.boot.service.PaymentService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

/**
 * Created by stefan on 27.12.17.
 */
@Configuration
@Import({PaymentService.class,
		PaymentRepository.class,
		})
public class PaymentApiConfig {

	@Bean
	PaymentResourceAssembler paymentResourceAssembler(){return new PaymentResourceAssembler();}

	@Bean
	PaymentController paymentController(){return new PaymentController();}

	@Bean
	public Jackson2ObjectMapperBuilder objectMapperBuilder() {
		Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
		builder.serializationInclusion(JsonInclude.Include.NON_NULL);
		builder.serializers(new MoneySerializer(),new InstantSerializer());
		builder.deserializers(new MoneyDeserializer(),new InstantDeserializer());
		return builder;
	}
}
