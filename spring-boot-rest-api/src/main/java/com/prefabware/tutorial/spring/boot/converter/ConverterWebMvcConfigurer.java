package com.prefabware.tutorial.spring.boot.converter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.FormatterRegistry;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * to register converter that are used to create instances of the required type
 * for @PathVariable and @RequestParam
 */
@Component
public class ConverterWebMvcConfigurer extends WebMvcConfigurerAdapter {
	@Autowired
	List<NameConverter> converter;
	@Override
	public void addFormatters(FormatterRegistry registry) {
		converter.stream().forEach(c->registry.addConverter(c));
	}
}