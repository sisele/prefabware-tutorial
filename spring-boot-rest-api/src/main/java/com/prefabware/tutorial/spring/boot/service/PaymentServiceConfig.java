package com.prefabware.tutorial.spring.boot.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by stefan on 27.12.17.
 */
@Configuration
public class PaymentServiceConfig {
	@Bean
	PaymentService paymentService(){return new PaymentService();}
}
