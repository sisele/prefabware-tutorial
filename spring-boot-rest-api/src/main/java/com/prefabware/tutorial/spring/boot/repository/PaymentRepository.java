package com.prefabware.tutorial.spring.boot.repository;

import java.util.LinkedHashMap;
import java.util.Map;

import com.prefabware.tutorial.spring.boot.domain.Payment;
import com.prefabware.tutorial.spring.boot.domain.PaymentId;

/**
 * a stub for a repository that persists in memory only
 * Created by stefan on 27.12.17.
 */
public class PaymentRepository {
	long nextId = 1;
	Map<PaymentId, Payment> map = new LinkedHashMap<>();

	PaymentId next(){
		PaymentId current = PaymentId.of(String.format("%010d", nextId));
		nextId++;//not threadsafe, dont do that in prod code
		return current;

	}

	public Payment save(Payment payment) {
		if (payment.id == null) {
			payment = new Payment(next(), payment.request, payment.paidAt);
		}
		map.put(payment.id, payment);
		return payment;
	}

	public Payment find(PaymentId id) {
		return map.get(id);
	}

	public Payment delete(PaymentId id) {
		return map.remove(id);
	}
}
