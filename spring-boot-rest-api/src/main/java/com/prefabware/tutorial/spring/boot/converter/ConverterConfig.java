package com.prefabware.tutorial.spring.boot.converter;

import java.util.List;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.support.DefaultFormattingConversionService;

/**
 * Created by stefan on 22.10.17.
 */

public class ConverterConfig {


	@Bean
	public NameConverter nameConverter() {
		NameConverter nameConverter = new NameConverter();
		return nameConverter;
	}

	@Bean
	public ConversionService conversionService(List<NameConverter> converter) {
		DefaultFormattingConversionService conversionService = new DefaultFormattingConversionService();
		// customize conversionService
		converter.stream().forEach(c->conversionService.addConverter(c));
		return conversionService;
	}


}
