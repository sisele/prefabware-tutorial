package com.prefabware.tutorial.spring.boot.repository;

import com.prefabware.tutorial.spring.boot.domain.Payment;
import com.prefabware.tutorial.spring.boot.domain.PaymentId;
import com.prefabware.tutorial.spring.boot.test.PaymentTestFactory;
import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by stefan on 03.01.18.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {PaymentRepositoryTest.Config.class})
public class PaymentRepositoryTest {
	@Configuration
	@Import({PaymentRepositoryConfig.class})
	static class Config {
	}

	@Autowired
	PaymentRepository paymentRepository;
	PaymentTestFactory factory ;

	@Before
	public void setUp() throws Exception {
		factory = new PaymentTestFactory();
	}

	@Test
	public void testNext() throws Exception {
		paymentRepository.nextId=123;
		PaymentId next = paymentRepository.next();
		assertEquals("0000000123",next.toString());
		assertEquals(124L,paymentRepository.nextId);
	}

	@Test
	public void saveWithIdNull() throws Exception {
		Payment payment = factory.payment(null);
		Payment saved = paymentRepository.save(payment);
		assertTrue("after save, a id should be set",saved.getId()!=null);
	}

	@Test
	public void saveWithIdNotNull() throws Exception {
		Payment payment = factory.payment(PaymentId.of("1234567890"));
		Payment saved = paymentRepository.save(payment);
		assertEquals(payment.id,saved.getId());
	}

	@Test
	public void testFind() throws Exception {
		PaymentId id = PaymentId.of("1234567890");
		Payment payment = factory.payment(id);
		Payment saved = paymentRepository.save(payment);
		Payment found = paymentRepository.find(id);
		assertEquals(found,saved);
	}

	@Test
	public void testDelete() throws Exception {
		PaymentId id = PaymentId.of("1234567890");
		Payment payment = factory.payment(id);
		Payment saved = paymentRepository.save(payment);
		Payment found = paymentRepository.find(id);
		assertEquals(found,saved);
		paymentRepository.delete(id);
		assertNull( paymentRepository.find(id));
	}
}