package com.prefabware.tutorial.spring.boot.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.*;

import static org.junit.Assert.*;

/**
 * https://www.paypalobjects.com/en_AU/vhelp/paypalmanager_help/credit_card_numbers.htm
 * Created by stefan on 27.12.17.
 */
public class CreditCardNumberTest {
ObjectMapper objectMapper=new ObjectMapper();
	@Test
	public void testMatches() throws Exception {
		assertTrue(CreditCardNumber.matches("378282246310005"));
		assertTrue(CreditCardNumber.matches("3782-8224-6310-005"));
		assertFalse(CreditCardNumber.matches("x782-8224-6310-005"));
	}

	@Test
	public void testOf() throws Exception {
		CreditCardNumber.of("378282246310005");
		CreditCardNumber.of("3782-8224-6310-005");

		assertThrows(()->CreditCardNumber.of("x782-8224-6310-005"),IllegalArgumentException.class,CreditCardNumber.NOT_A_VALID_CREDIT_CARD_NUMBER);
	}

	@Test
	public void testSerialize() throws Exception {
		CreditCardNumber cc = CreditCardNumber.of("3782-8224-6310-005");
		String json = objectMapper.writeValueAsString(cc);
		assertEquals("\"3782-8224-6310-005\"",json);
	}

	@Test
	public void testDeserialize() throws Exception {
		CreditCardNumber cc = objectMapper.readValue("\"3782-8224-6310-005\"",CreditCardNumber.class);
		assertEquals("3782-8224-6310-005",cc.toString());
	}

	void assertThrows(Runnable runnable, Class<? extends Exception> clazz, CharSequence message) {
		try {
			runnable.run();
		}catch (Exception e){
			assertEquals(clazz,e.getClass());
			assertTrue(e.getMessage().contains(message));
		}
	}
}