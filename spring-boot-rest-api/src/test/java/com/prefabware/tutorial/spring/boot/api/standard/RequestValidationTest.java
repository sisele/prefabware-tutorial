package com.prefabware.tutorial.spring.boot.api.standard;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.junit.*;
import org.junit.runner.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * to test the default request validation performed by spring
 * @author stefan
 *
 */
@RunWith(SpringRunner.class)
//removing theSpringBootEmptyApplication makesmany test fail
//looks like it adds an exception handler...
@SpringBootTest(classes = { SpringBootTestApplication.class,RequestValidationTest.Config .class })
@WebAppConfiguration
public class RequestValidationTest {
	public static final String X_INT_ROLE = "x-int-role";
	/**
	 * the @RequestMapping on the controller is used as a default for the
	 * @RequestMapping on the controller methods.
	 * So all methods will only produce application/json
	 */
	@RestController
	@RequestMapping(produces= MediaType.APPLICATION_JSON_VALUE)
	public static class ValidatedController {
		public static final String ROLE_DEPENDENT = "/role-dependant";

		@Autowired
		HttpSession session;
		public int called;

		@RequestMapping(method = RequestMethod.GET,value = "/some-endpoint")
		public String get(@RequestParam(required = false) @Valid String dummy) {
			called++;
			return "RequestValidationTest : " + called;
		}
		@RequestMapping(method = RequestMethod.GET,value = "/int-role")
		public String getOnlyWithInt(@RequestHeader(name=X_INT_ROLE)String role) {
			called++;
			return "RequestValidationTest : " + called;
		}

		/**
		 * mapped only to requests with header x-int-role=admin
         * @return
         */
		@RequestMapping(method = RequestMethod.GET,value = ROLE_DEPENDENT, headers = {"x-int-role=admin"})
		public String onlyAdmin() {
			called++;
			return "RequestValidationTest : " + called;
		}

		@RequestMapping(method = RequestMethod.GET,value = "/versioned", headers = {"Accept=application/json","v=2.0"})
		public String version2() {
			called++;
			return "version 2 : " + called;
		}

		@RequestMapping(method = RequestMethod.GET,value = "/versioned", headers = {"Accept=application/json","v=1.0"})
		public String version1() {
			called++;
			return "version 1 : " + called;
		}

		@RequestMapping(method = RequestMethod.POST,value = "/some-endpoint")
		public String post(@RequestBody() Body body) {
			called++;
			return "RequestValidationTest : " + called;
		}
		@RequestMapping(method = RequestMethod.GET,value = "/with-cookie-only")
		public String getWithCookieOnly(@CookieValue(name = "key") String value) {
			called++;
			return "RequestValidationTest : " + called;
		}
		@InitBinder
		public void initBinder(WebDataBinder binder) {
			return;
		}

	}
static class Body{
	String name;
}
	@Configuration
	static class Config {

		@Bean
		ValidatedController validatedController() {
			return new ValidatedController();
		}

	}

	Logger LOGGER = LoggerFactory.getLogger(RequestValidationTest.class);

	MockMvc mockMvc;

	@Autowired
	WebApplicationContext ctx;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(ctx)
				.build();
	}
	/**
	 * as per HTTP spec :
	 * If no Accept header field is present, then it is assumed that the client accepts all media types
	 * @throws Exception
	 */
	@Test
	public void testMissingAcceptHeaderOk() throws Exception {

		mockMvc.perform(get("/some-endpoint"))
				.andDo(print()).andExpect(status().isOk());

	}
	/**
	 * as per HTTP spec :
	 *  If an Accept header field is present, and if the server cannot send a response which is acceptable according to the combined Accept field value,
	 *  then the server SHOULD send a 406 (not acceptable) response.
	 * @throws Exception
     */
	@Test
	public void testNotAcceptable406() throws Exception {
		mockMvc.perform(get("/some-endpoint")
				.accept(MediaType.parseMediaType("application/xxxxx"))
		)
				.andDo(print()).andExpect(status().isNotAcceptable());
	}

	/**
	 * as per HTTP spec
	 * a Http 405 should be returned when ...
	 * The method specified in the Request-Line is not allowed for the resource identified by the Request-URI.
	 * @throws Exception
	 */
	@Test
	public void testMethodNotAllowed405() throws Exception {
		mockMvc.perform(delete("/some-endpoint"))
				.andDo(print()).andExpect(status().isMethodNotAllowed());
	}

	/**
	* as per HTTP spec
	* a Http 404 should be returned when ...
	* The server has not found anything matching the Request-URI
	* @throws Exception
	*/
	@Test
	public void testNotFound404() throws Exception {
		mockMvc.perform(get("/some-non-existing-endpoint"))
				.andDo(print()).andExpect(status().isNotFound());
	}
	/**
	 * if a wrong uri and a wrong accept header is supplied
	 * 404 is returned
	 * @throws Exception
	 */
	@Test
	public void testNotFound404BeforeWronAcceptHeeader() throws Exception {
		mockMvc.perform(get("/some-non-existing-endpoint")
				.accept(MediaType.parseMediaType("application/xxxxx")))
				.andDo(print()).andExpect(status().isNotFound());
	}
	/**
	 *
	 * if a wrong uri and a wrong Http method is supplied
	 * in case there is no controller method mapped to the given Http method at all
	 * 405 is returned
	 * @throws Exception
	 */
	@Test
	public void testWrongMethod405BeforeNotFound() throws Exception {
		mockMvc.perform(delete("/some-non-existing-endpoint"))
				.andDo(print()).andExpect(status().isNotFound());
	}
	/**
	 * as per HTTP spec
	 * a Http 400 should be returned when ...
	 * The request could not be understood by the server due to malformed syntax
	 * @throws Exception
	 */
	@Test
	public void testBadRequest400() throws Exception {
		mockMvc.perform(post("/some-endpoint")		)
				.andDo(print()).andExpect(status().isBadRequest());
	}
	/**
	 * to test that a post is succesfull
	 * @throws Exception
	 */
	@Test
	public void testPostOk() throws Exception {
		Body b=new Body();
		mockMvc.perform(post("/some-endpoint")
				.content("{\"name\":\"value\"}")
		         .contentType(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isOk());
	}
	/**
	 * if no Content-Type is specified, the
	 * @throws Exception
	 */
	@Test
	public void testNoContentTypeUnsupportedMediaType415() throws Exception {
		Body b=new Body();
		mockMvc.perform(post("/some-endpoint")
				.content("{\"name\":\"value\"}")
		)
				.andDo(print()).andExpect(status().isUnsupportedMediaType());
	}
	/**
	 * as per HTTP spec
	 * a Http 415 should be returned when ...
	 * The server is refusing to service the request because the entity of the request
	 * is in a format not supported by the requested resource for the requested method.
	 * @throws Exception
	 */
	@Test
	public void testUnsuportedMediaType415() throws Exception {
		Body b=new Body();
		mockMvc.perform(post("/some-endpoint")
				.content("{\"name\":\"value\"}")
				.contentType(MediaType.APPLICATION_XML))
				.andDo(print()).andExpect(status().isUnsupportedMediaType());
	}

	/**
	 * if a wrong content type is specified in the request
	 * and a wrong 'Accept' header is supplied,
	 * Http Status Not Acceptible will be returned
	 * @throws Exception
     */
	@Test
	public void testIsNotAccptableBeforeUnsuportedMediaType415() throws Exception {
		Body b=new Body();
		mockMvc.perform(post("/some-endpoint")
				.accept(MediaType.parseMediaType("application/xxxxx"))
				.content("{\"name\":\"value\"}")
				.contentType(MediaType.APPLICATION_XML))
				.andDo(print()).andExpect(status().isNotAcceptable());
	}


	/**
	 * if the controller method maps a parameter to a cookie,
	 * it must be supplied
	 * @throws Exception
	 */
	@Test
	public void testMandatoryCookieOk() throws Exception {
		Cookie c = new Cookie("key","value");
		mockMvc.perform(get("/with-cookie-only").cookie(c))

				.andDo(print()).andExpect(status().isOk());
	}

	/**
	 * if the controller method maps a parameter to a cookie,
	 * and that is not supplied with the request
	 * a Http status 400 is returned
	 * @throws Exception
	 */
	@Test
	public void testMandatoryCookieMissingBadRequest400() throws Exception {
		mockMvc.perform(get("/with-cookie-only"))
				.andDo(print())
				.andExpect(status()
						.isBadRequest());
	}

		/**
         * if no controller method is mapped for the request parameters
         * and the Accept header is wrong,
         * a Http status 406 is returned
         * @throws Exception
         */
	@Test
	public void testWrongParameterbeforeAcceptHeader406() throws Exception {
		mockMvc.perform(get("/some-endpoint",5L)
				.accept(MediaType.parseMediaType("application/xxxxx")))
				.andDo(print()).andExpect(status().isNotAcceptable());
	}
	/**
	 * a missing but required header is answered by a Http400 Bad Reqeust
     * (a Http412 Preconditions Not Met might be reasonable too, but most webserver respond with Http400, and spring also)
	 * @throws Exception
	 */
	@Test
	public void testMissingRequestHeaderBadRequest400() throws Exception {
		mockMvc.perform(get("/int-role")		)
				.andDo(print()).andExpect(status().isBadRequest());
	}
	@Test
	public void testRequiredRequestHeaderValueOk() throws Exception {
		mockMvc.perform(get(ValidatedController.ROLE_DEPENDENT).header(X_INT_ROLE,"admin")
		)
				.andDo(print()).andExpect(status().isOk());
	}
	@Test
	public void testRequiredRequestHeaderValueMissing404() throws Exception {
		mockMvc.perform(get(ValidatedController.ROLE_DEPENDENT)		)
				.andDo(print()).andExpect(status().isNotFound());
	}
	@Test
	public void testRequiredRequestHeaderValueWrongOk() throws Exception {
		mockMvc.perform(get(ValidatedController.ROLE_DEPENDENT).header(X_INT_ROLE,"wrong")		)
				.andDo(print()).andExpect(status().isNotFound());
	}
}
