package com.prefabware.tutorial.spring.boot.domain;

import java.time.YearMonth;

import org.junit.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by stefan on 27.12.17.
 */
public class ExpiryTest {

	@Test
	public void testOfStringOk() throws Exception {
		Expiry of = Expiry.of("07/17");
		assertEquals(2017,of.yearMonth.getYear());
		assertEquals(7,of.yearMonth.getMonthValue());
	}

	@Test
	public void testOfYearMonthOk() throws Exception {
		Expiry of = Expiry.of(YearMonth.of(2017,8));
		assertEquals(2017,of.yearMonth.getYear());
		assertEquals(8,of.yearMonth.getMonthValue());
	}
}