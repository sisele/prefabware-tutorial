package com.prefabware.tutorial.spring.boot.domain;

import java.math.BigDecimal;
import javax.money.CurrencyUnit;
import javax.money.Monetary;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.prefabware.tutorial.spring.boot.domain.jackson.MoneyDeserializer;
import com.prefabware.tutorial.spring.boot.domain.jackson.MoneySerializer;
import org.javamoney.moneta.Money;
import org.junit.*;

import static org.junit.Assert.*;

public class MoneyTest {
	ObjectMapper mapper;
	CurrencyUnit usd;
	Money twelveUsd;

	@Before
	public void setUp() throws Exception {
		usd = Monetary.getCurrency("USD");
		twelveUsd = Money.of(12.34, usd);
		mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addSerializer(Money.class, new MoneySerializer());
		module.addDeserializer(Money.class, new MoneyDeserializer());
		mapper.registerModule(module);

	}

	@Test
	public void testSerialize() throws Exception {
		String json = mapper.writeValueAsString(twelveUsd);
		assertEquals("{\"currency\":\"USD\",\"value\":\"12.34\"}", json);
	}

	@Test
	public void testDeserialize() throws Exception {
		Money cc = mapper.readValue("{\"currency\":\"USD\",\"value\":\"12.34\"}", Money.class);
		assertEquals("USD", cc.getCurrency()
				.getCurrencyCode());
		assertEquals(new BigDecimal("12.34"), cc.getNumber()
				.numberValue(BigDecimal.class));
	}


}