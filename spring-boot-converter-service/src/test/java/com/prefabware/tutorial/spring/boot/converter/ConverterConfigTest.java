package com.prefabware.tutorial.spring.boot.converter;

import java.lang.reflect.Field;

import org.junit.*;

import static org.junit.Assert.assertNotNull;

/**
 * Created by stefan on 20.03.18.
 */
public class ConverterConfigTest {
	@Test
	public void testName() throws Exception {
		Class<?> outer = Class.forName(
				"org.springframework.hateoas.mvc.AnnotatedParametersParameterAccessor");
		Class<?> aClass = Class.forName(
				"org.springframework.hateoas.mvc.AnnotatedParametersParameterAccessor$BoundMethodParameter");
		Field field = aClass.getDeclaredField("CONVERSION_SERVICE");
		field.setAccessible(true);
		Object o = field.get(null);
		assertNotNull(o);

	}
}