package com.prefabware.tutorial.spring.boot.web;

import java.time.Duration;

import com.prefabware.tutorial.spring.boot.converter.Name;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class Controller {
    @Value("${the.name}")
    Name name;
    @RequestMapping("/duration/{duration}")
    public String index(@PathVariable("duration") Duration duration) {
        return "Greetings from Spring Boot!";
    }
    @RequestMapping("/name/{name}")
    public String name(@PathVariable("name") Name name) {
        return "Name : "+name;
    }

    @RequestMapping("/name")
    public String query(@RequestParam("name") Name name) {
        return "Name : "+name;
    }
}