package com.prefabware.tutorial.spring.boot.converter;

import java.lang.reflect.Field;

import org.springframework.core.convert.converter.Converter;
import org.springframework.format.support.DefaultFormattingConversionService;

/**
 * Created by stefan on 20.03.18.
 */
public class AnnotatedParametersParameterAccessorCustomizer {
	final DefaultFormattingConversionService service;
	Class<?> outer = forName(
			"org.springframework.hateoas.mvc.AnnotatedParametersParameterAccessor");

	Class<?> aClass = forName(
			"org.springframework.hateoas.mvc.AnnotatedParametersParameterAccessor$BoundMethodParameter");

	AnnotatedParametersParameterAccessorCustomizer() {
		try {
			Field field = aClass.getDeclaredField("CONVERSION_SERVICE");
			field.setAccessible(true);
			service = (DefaultFormattingConversionService) field.get(null);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	static Class<?> forName(String name) {
		try {
			return Class.forName(name);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public void addConverter(Converter<?, ?> converter) {
		service.addConverter(converter);
	}
}
