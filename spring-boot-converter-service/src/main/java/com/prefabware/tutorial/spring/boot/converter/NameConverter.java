package com.prefabware.tutorial.spring.boot.converter;

import java.time.Duration;

import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@ConfigurationPropertiesBinding
/**
 * Created by stefan on 22.10.17.
 */
public class NameConverter implements Converter<String,Name> {
	@Override
	public Name convert(String source) {
		return new Name(source);
	}
}
