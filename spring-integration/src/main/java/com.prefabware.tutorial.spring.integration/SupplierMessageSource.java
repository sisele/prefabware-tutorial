package com.prefabware.tutorial.spring.integration;

import java.util.function.Supplier;

import org.springframework.integration.endpoint.AbstractMessageSource;

/**
 * Created by stefan on 17.10.17.
 */
class SupplierMessageSource<T> extends AbstractMessageSource<T> {
	final Supplier<T> supplier;

	SupplierMessageSource(Supplier<T> supplier) {
		this.supplier = supplier;
	}

	@Override
	protected Object doReceive() {
		return supplier.get();
	}

	@Override
	public String getComponentType() {
		return "inbound-channel-adapter";
	}
}
