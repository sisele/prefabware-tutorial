package com.prefabware.tutorial.spring.integration;

import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.ExecutorChannel;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dispatcher.LoadBalancingStrategy;
import org.springframework.integration.dispatcher.RoundRobinLoadBalancingStrategy;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.channel.MessageChannels;
import org.springframework.integration.dsl.core.Pollers;
import org.springframework.integration.endpoint.AbstractMessageSource;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by stefan on 06.10.17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=MultipleListenerTest.Config.class)
public class MultipleListenerTest {

	static class Config{
		@Bean
		public MessageSource<?> integerMessageSource() {
			AtomicInteger atomic = new AtomicInteger();
			SupplierMessageSource<Integer> source = new SupplierMessageSource<Integer>(atomic::getAndIncrement);
			return source;
		}

		@Bean
		public ExecutorChannel inputChannel() {
			Executor executor=new SimpleAsyncTaskExecutor();
			LoadBalancingStrategy strategy=new RoundRobinLoadBalancingStrategy();
			return new ExecutorChannel(executor,strategy);
		}

		@Bean
		public IntegrationFlow myFlow() {
			return IntegrationFlows.from(this.integerMessageSource(), c ->
					c.poller(Pollers.fixedRate(100)))
					.channel(this.inputChannel())
					.filter((Integer p) -> p > 0)
					.transform(Object::toString)
					.channel(MessageChannels.queue())
					.get();
		}
	}
	static class SupplierMessageSource<T> extends AbstractMessageSource<T> {
		final Supplier<T> supplier;

		SupplierMessageSource(Supplier<T> supplier) {
			this.supplier = supplier;
		}

		@Override
		protected Object doReceive() {
			return supplier.get();
		}

		@Override
		public String getComponentType() {
				return "inbound-channel-adapter";
		}
	}


	@Test
	public void test() throws Exception {


	}
}
