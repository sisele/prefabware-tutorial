package com.prefabware.tutorial.spring.integration;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.channel.MessageChannels;
import org.springframework.integration.dsl.core.Pollers;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by stefan on 06.10.17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=SupplierMessageSourceTest.Config.class)
public class SupplierMessageSourceTest {
	static class Config{
		@Bean
		public MessageSource<?> integerMessageSource() {
			AtomicInteger atomic = new AtomicInteger();
			SupplierMessageSource<Integer> source = new SupplierMessageSource<Integer>(atomic::getAndIncrement);
			return source;
		}

		@Bean
		public DirectChannel inputChannel() {
			return new DirectChannel();
		}

		@Bean
		public IntegrationFlow myFlow() {

			AtomicInteger atomic = new AtomicInteger();

			return IntegrationFlows.from(this.integerMessageSource(), c ->
					c.poller(Pollers.fixedRate(100)))
					.channel(this.inputChannel())
					.filter((Integer p) -> p > 0)
					.transform(Object::toString)
					.channel(MessageChannels.queue())
					.get();
		}
	}


	@Test
	public void test() throws Exception {


		
	}
}
