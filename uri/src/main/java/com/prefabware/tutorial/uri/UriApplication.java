package com.prefabware.tutorial.uri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UriApplication {

	public static void main(String[] args) {
		SpringApplication.run(UriApplication.class, args);
	}
}
