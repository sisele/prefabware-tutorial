package com.prefabware.tutorial.uri;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import static org.junit.Assert.*;

public class UriTest {

	/**
	 * if the URI contains a '=' as value of a query parameter, build(false) must be used
	 * build(true) throws an exception - Invalid character '=' for QUERY_PARAM in "xyz="
	 * which is wrong, cause a '=' in the query value is allowed regarding RFC
	 */
	@Test
	public void equalAsQueryRequiresBildFalse() {
		String expected = "example.com?equal=xyz=";
		URI uri = URI.create(expected);
		UriComponents buildFalse = UriComponentsBuilder
				.fromUri(uri)
				.build(false);
		assertEquals(uri, buildFalse.toUri());
		assertEquals(expected, buildFalse.toUriString());
	}

	@Test
	public void spaceAsQueryValueRequiresBuildTrue() throws Exception {
		//when creating an URI from string,the query must be encoded
		URI uri = new URI("https://example.com?key1=val1%20val2");

		UriComponents builder = UriComponentsBuilder
				.fromUri(uri)
				.build(true);
		assertEquals(uri, builder.toUri());//this fails on build=false
		assertEquals("https://example.com?key1=val1%20val2", builder.toUriString());
	}

	/**
	 * for the '=' false is required, for the ' ' true is required, so
	 * its just not possible
	 *
	 * @throws Exception
	 */
	@Test
	public void spaceAndEqualsAsQueryValueNotPossible() throws Exception {
		//when creating an URI from string,the query must be encoded
		URI uri = new URI("https://example.com?space=a%20b&equal==");

		UriComponents builder = UriComponentsBuilder
				.fromUri(uri)
				.build(false);
		//assertEquals(uri, builder.toUri());//this fails on build=false
		assertEquals("https://example.com?space=a%20b&equal==", builder.toUriString());
	}


	@Test
	public void uri() throws Exception {
		URI uri = new URI("https://example.com?space=a%20b&equal==&amp=&&plus=+");
	}

	@Test
	public void uriOnly() throws Exception {
		URI uri = new URI("https", null, "example.com", -1, null, "space=a b&equal==&plus=+", null);
		assertEquals("https://example.com?space=a%20b&equal==&plus=+", uri.toString());
		assertEquals("space=a b&equal==&plus=+", uri.getQuery());

		URI uriFromString = URI.create(uri.toString());

		assertEquals(uri, uriFromString);
	}

	@Test
	public void uriEqualEncoded() throws Exception {
		URI uri = URI.create("https://example.com?equal=a%3Db");
		assertEquals("equal=a=b", uri.getQuery());
	}

	@Test
	public void testKeyValue() throws Exception {
		UriModifier.QueryParm queryParm = new UriModifier.QueryParm("space=a b");
		assertEquals("space", queryParm.key);
		assertEquals("a b", queryParm.value);
	}

	@Test
	public void emptyQuery() throws Exception {
		UriModifier.Query query=new UriModifier.Query();

		assertEquals("", query.toString());
	}


	@Test
	public void uriB() throws Exception {
		URI uri = new URI("https", null, "example.com", -1, null, "space=a b&equal==&plus=+", null);
		assertEquals("https://example.com?space=a%20b&equal==&plus=+", uri.toString());
		assertEquals("space=a b&equal==&plus=+", uri.getQuery());

		UriModifier b = new UriModifier(uri);
		UriModifier.Query query = b.query();
		assertEquals(3, query.size());
		assertEquals("space", query.get(0).key);
		assertEquals("a b", query.get(0).value);
		assertEquals("equal", query.get(1).key);
		assertEquals("=", query.get(1).value);
		assertEquals("plus", query.get(2).key);
		assertEquals("+", query.get(2).value);
	}

	@Test
	public void uriChange() throws Exception {
		URI uri = URI.create("https://example.com?a=1&b=2");
		URI c = UriModifier.create(uri)
				.addQuery("space", "3 4")
				.addQuery("plus", "5+6")
				.addQuery("equal", "6=7")
				.uri();
		assertEquals("https://example.com?a=1&b=2&space=3%204&plus=5+6&equal=6=7",c.toString());
		;
	}

	@Test
	public void fromEmpty() throws Exception {
		URI c = UriModifier.create()
				.schema("https")
				.host("example.com")
				.clearQuery()
				.addQuery("space", "3 4")
				.addQuery("plus", "5+6")
				.addQuery("equal", "6=7")
				.uri();
		assertEquals("https://example.com?space=3%204&plus=5+6&equal=6=7",c.toString());
	}

	@Test
	public void spaceInPath() throws Exception {
		URI c = UriModifier.create()
				.schema("https")
				.host("example.com")
				.path("/pa th")
				.uri();
		assertEquals("https://example.com/pa%20th",c.toString());
	}

	@Test
	public void forcePlusEncoded() throws Exception {
		URI c = UriModifier.create()
				.schema("https")
				.host("example.com")
				.addQuery("plus", "5%3D6")
				.uri();

		assertEquals("https://example.com?plus=5%253D6",c.toString());
	}

	public static class UriModifier {

		String schema;
		String userInfo;
		String host;
		int port=-1;
		String path;
		Query query;
		String fragment;

		static UriModifier create(URI uri) {
			return new UriModifier(uri);
		}
		static UriModifier create() {
			return new UriModifier();
		}

		URI uri() {
			try {
				String query = query().toString();
				return new URI(schema, userInfo, host, port, path, query.isEmpty()?null:query, fragment);
			} catch (URISyntaxException e) {
				throw new RuntimeException(e);
			}
		}

		public String fragment() {
			return fragment;
		}

		public String host() {
			return host;
		}

		public String path() {
			return path;
		}

		public int port() {
			return port;
		}

		public String schema() {
			return schema;
		}

		public String userInfo() {
			return userInfo;
		}

		public UriModifier fragment(String fragment) {
			this.fragment = fragment;
			return this;
		}

		public UriModifier host(String host) {
			this.host = host;
			return this;
		}

		public UriModifier path(String path) {
			this.path = path;
			return this;
		}

		public UriModifier port(int port) {
			this.port = port;
			return this;
		}

		public UriModifier schema(String schema) {
			this.schema = schema;
			return this;
		}

		public UriModifier userInfo(String userInfo) {
			this.userInfo = userInfo;
			return this;
		}

		UriModifier clearQuery() {
			this.query.clear();
			return this;
		}

		public static class Query {
			public Query(List<QueryParm> list) {
				this.list = list;
			}

			List<QueryParm> list = new ArrayList<>();

			public Query() {

			}

			public boolean add(QueryParm queryParm) {
				return list.add(queryParm);
			}

			public void clear() {
				list.clear();
			}

			public boolean addAll(Collection<? extends QueryParm> c) {
				return list.addAll(c);
			}

			public boolean isEmpty() {
				return list.isEmpty();
			}

			public int size() {
				return list.size();
			}

			public Stream<QueryParm> stream() {
				return list.stream();
			}

			public QueryParm get(int index) {
				return list.get(index);
			}

			void add(String key, String value) {
				add(new QueryParm(key, value));
			}

			@Override
			public String toString() {
				return stream().map(p -> p.toString())
						.collect(Collectors.joining("&"));
			}
		}

		public static class QueryParm {
			/**
			 * to split key and value at '=', while not splitting at '=' in the value
			 */
			Pattern pattern = Pattern.compile("([^=]*)=(.*)");
			public final String key;
			public final String value;

			QueryParm(String keyValue) {
				if (!keyValue.contains("=")) {
					//key only
					this.key = keyValue;
					this.value = null;
				} else {
					Matcher matcher = pattern.matcher(keyValue);
					if (matcher.matches()) {
						this.key = matcher.group(1);
						this.value = matcher.group(2);
					} else {
						throw new IllegalArgumentException(keyValue + " is not a valid query parameter");
					}
				}
			}

			public QueryParm(String key, String value) {
				this.key = key;
				this.value = value;
			}

			@Override
			public String toString() {
				return key + "=" + value;
			}
		}

		public UriModifier() {
			this.uri=null;
			this.query = createQuery(null);
		}
		public UriModifier(URI uri) {
			this.uri = uri;
			this.schema = uri.getScheme();
			this.userInfo = uri.getUserInfo();
			this.host = uri.getHost();
			this.port = uri.getPort();
			this.path = uri.getPath();
			this.query = createQuery(uri);
			this.fragment = uri.getFragment();
		}

		final URI uri;

		/**
		 * @return a List containing the unencoded key/valu pairs of the query
		 */
		public Query query() {
			return this.query;
		}

		Query createQuery(URI uri) {
			if(uri==null){return new Query();}
			String query = uri.getQuery();
			if(query==null){return new Query();}
			String[] split = query
					.split("&");
			return new Query(Arrays.stream(split)
					.map(kv -> new QueryParm(kv))
					.collect(Collectors.toList()));
		}

		public UriModifier addQuery(String key, String value) {
			this.query()
					.add(key, value);
			return this;
		}

	}
}
