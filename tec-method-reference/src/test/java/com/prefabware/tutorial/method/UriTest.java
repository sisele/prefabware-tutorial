package com.prefabware.tutorial.method;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import static org.junit.Assert.assertEquals;

public class UriTest {
	static class Handler {
		<T> T call(T t) {
			return t;
		}
	}

	@Test
	public void methodRef() {
		Integer i = new Integer(123);
		Handler h = new Handler();
		h.call(i).intValue();
	}
}
